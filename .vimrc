set encoding=utf-8
scriptencoding utf-8
setglobal fileencoding=utf-8

set hidden

" Indent options {{{
set tabstop=4
set softtabstop=4
set shiftwidth=4
set backspace=indent,eol,start
set autoindent
set noexpandtab
set nowrap
set noendofline
" }}}

" Numbering
set number
set relativenumber
set noshowmode
set signcolumn="yes"

" Mouse
set mouse=a
set mousehide

" Bells
set novisualbell
set noerrorbells

" List options
set list
set listchars=
" set listchars+=tab:→\  " U+2192
set listchars+=tab:│\ 
set listchars+=trail:·
set listchars+=extends:»
set listchars+=precedes:«

" Fill chars
set fillchars=
set fillchars+=vert:│
set fillchars+=fold:\ 
set fillchars+=diff:-

if &term ==? 'rxvt-unicode-256color' || &term ==? 'screen'
	" Most color schemes don't play well with urxvt when termguicolors is set
	set notermguicolors
	set t_Co=256
else
	set termguicolors
endif
set t_ut=

" Backup options {{{
set nobackup
set nowritebackup
set noswapfile
" }}}

" Fold options {{{
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=99
" }}}

" Statusline options {{{
set laststatus=2
set statusline=\ [%{mode()}\|%03n]\ %f\ %{fugitive#statusline()}\ %m%r%h%w\ %=\ [%{&ft}\|%{&ff}\|%{(&fenc!=''?&fenc:&enc)}]%{statusline#ale()}(%l-%L):%c\ 
" }}}

" File-formats and encoding {{{
set fileformat=unix
set fileformats=unix,dos,mac
" }}}

" Split settings {{{
set splitright
set splitbelow
" }}}

" Search options {{{
set ignorecase
set showmatch
set smartcase
set hlsearch
set incsearch
set synmaxcol=400
" }}}

" Completion
set completeopt=menu,menuone,longest,preview
set pumheight=12


" Leader binding
let g:mapleader = ','

" Clear search highlight on space and clear the prompt
noremap <silent> <space> :nohlsearch<bar>:let @/=''<cr>

" Ecscape binding
inoremap jk <Esc>
inoremap kj <Esc>

filetype off

" Plugins for the people {{{
if has('win32')
	call plug#begin('~/vimfiles/plugged')
else
	call plug#begin('~/.vim/plugged')
endif

" Languages {{{
Plug 'udalov/kotlin-vim'
Plug 'fatih/vim-go'
Plug 'rust-lang/rust.vim'
Plug 'vim-jp/vim-cpp'
Plug 'arrufat/vala.vim'
Plug 'sudar/vim-arduino-syntax'
Plug 'LaTeX-Box-Team/LaTeX-Box'
Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'elzr/vim-json'
Plug 'cakebaker/scss-syntax.vim'
Plug 'plasticboy/vim-markdown'
Plug 'cespare/vim-toml'
Plug 'leafgarland/typescript-vim'
Plug 'stephpy/vim-yaml'
Plug 'derekwyatt/vim-sbt'
Plug 'derekwyatt/vim-scala'
Plug 'kchmck/vim-coffee-script'
Plug 'tpope/vim-git'
Plug 'pangloss/vim-javascript'
Plug 'kylef/apiblueprint.vim'
Plug 'aklt/plantuml-syntax'
Plug 'lervag/vimtex'
Plug 'PProvost/vim-ps1'
Plug 'OrangeT/vim-csharp'
Plug 'dart-lang/dart-vim-plugin'
" }}}

" Language tools {{{
Plug 'racer-rust/vim-racer'
Plug 'ensime/ensime-vim'
Plug 'Quramy/tsuquyomi', { 'do': 'make' }
Plug 'OmniSharp/omnisharp-vim'

let g:lsp_postupdate_hook_cmd = has('win32') || has('win64') ? 'powershell install.ps1' : 'bash install.sh'
Plug 'autozimu/LanguageClient-neovim', { 'branch': 'next', 'do': g:lsp_postupdate_hook_cmd }
" }}}

" Language lint {{{
Plug 'w0rp/ale'
" }}}

" Completions {{{
Plug 'Shougo/deoplete.nvim'
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'

Plug 'zchee/deoplete-go', { 'do': 'make' }
Plug 'rudism/deoplete-tsuquyomi'
Plug 'carlitux/deoplete-ternjs'
Plug 'Shougo/neco-vim'
" }}}

" Snippets {{{
Plug 'Shougo/neosnippet.vim'
Plug 'Shougo/neosnippet-snippets'
" }}}

" Colorschemes {{{
Plug 'NLKNguyen/papercolor-theme'
Plug 'rakr/vim-one'
Plug 'ayu-theme/ayu-vim'
Plug 'morhetz/gruvbox'
Plug 'ajh17/Spacegray.vim'
Plug 'arcticicestudio/nord-vim'
Plug 'blueshirts/darcula'
" }}}

" File/Code Exploration {{{
Plug 'scrooloose/nerdtree'
Plug 'majutsushi/tagbar'
Plug 'terryma/vim-multiple-cursors'
" }}}

" SCM {{{
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
" }}} 

Plug 'tpope/vim-commentary'
Plug 'google/vim-searchindex'

Plug 'tomtom/tlib_vim'
Plug 'MarcWeber/vim-addon-mw-utils'
Plug 'Shougo/vimproc.vim', { 'do': 'make' }
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'editorconfig/editorconfig-vim'
call plug#end()
" }}}

" Some options to reduce slowdown {{{
set norelativenumber
" set nocursorline
set ttyfast
set lazyredraw
" }}}

" Syntax {{{
syntax on
filetype plugin indent on
" }}}

" vim-go
let g:go_disable_autoinstall = 0

" NeoSnippet configuration {{{
imap <C-k> <Plug>(neosnippet_expand_or_jump)
smap <C-k> <Plug>(neosnippet_expand_or_jump)
xmap <C-k> <Plug>(neosnippet_expand_target)
" }}}

" Colorscheme {{{
let s:color_pre_scripts = {
	\ "ayu": [
		\ "let g:ayucolor = 'mirage'"
	\],
	\ "seoul256": [
		\ "let g:seoul256_background = 252"
	\],
	\ "spacegray": [
		\ "let g:spacegray_low_contrast=1"
	\],
	\ "gruvbox": [
		\ "let g:gruvbox_contrast_dark='medium'"
	\]
\}

let s:color_post_scripts = {
	\ "darcula": [
		\ "hi StatusLine term=reverse cterm=None ctermfg=67 ctermbg=249 gui=None guifg=#a9b7c6 guibg=#464646",
		\ "hi StatusLineNC term=reverse cterm=None ctermfg=241 ctermbg=238 gui=None guifg=#6a6e72 guibg=#3c3c3c"
	\]
\}

try
	set background=dark
	let s:color_to_set = "one"

	" Run pre scripts
	if has_key(s:color_pre_scripts, s:color_to_set)
		for s:command in s:color_pre_scripts[s:color_to_set]
			exe s:command
		endfor
	endif

	exe "colorscheme " . s:color_to_set
	
	" Run post scripts
	if has_key(s:color_post_scripts, s:color_to_set)
		for s:command in s:color_post_scripts[s:color_to_set]
			exe s:command
		endfor
	endif
catch
	"set t_Co=8
	colorscheme desert
endtry
" }}}


" NERDTree {{{
map <silent> <F2> :NERDTreeToggle<CR> " Toggle with F2
let g:NERDTreeShowHidden=1 " Show hidden files
let g:NERDTreeStatusline="%{exists('b:NERDTree')?'NERDTree':''}"
" }}}

" Tagbar {{{
nnoremap <silent> <F8> :TagbarToggle<CR>
let g:tagbar_iconchars = ['+', '-']
" }}}

" <TAB>: completion {{{
inoremap <expr><tab>  pumvisible() ? "\<c-n>" : "\<tab>"
inoremap <expr><s-tab> pumvisible() ? "\<c-p>" : "\<s-tab>"
" }}}

" Deoplete completions {{{
call deoplete#enable()

let g:deoplete#sources#ternjs#types = 1
let g:deoplete#sources#ternjs#docs = 1
let g:deoplete#sources#ternjs#filter = 0
let g:deoplete#sources#ternjs#case_insensitive = 1
let g:deoplete#sources#ternjs#sort = 0
" }}}

" Enable omni completion {{{
augroup OmniCompletions
	autocmd!
	autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
	autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
	autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
	autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
	autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
	autocmd FileType c setlocal omnifunc=ccomplete#Complete
	autocmd FileType sql setlocal omnifunc=sqlcomplete#Complete
	autocmd FileType java setlocal omnifunc=javacomplete#Complete
	autocmd FileType typescript setlocal completeopt+=menu,preview
augroup END
" }}}

" Spell check {{{
augroup SpellCheck
	autocmd!
	autocmd FileType tex setlocal spell spelllang=en_ca
	autocmd FileType org setlocal spell spelllang=en_ca
	autocmd FileType markdown setlocal spell spelllang=en_ca
augroup END
" }}}

" Kotlin {{{
augroup KotlinFiletype
	autocmd!
	autocmd FileType kotlin setlocal formatoptions-=t
	autocmd FileType kotlin setlocal formatoptions+=cronqlj
augroup END
" }}}

" .NET {{{
augroup DotnetAuCommands
	autocmd!
	autocmd BufNewFile,BufRead *.cs setlocal expandtab
	autocmd FileType cs setlocal expandtab
	autocmd BufNewFile,BufRead *.csproj setlocal expandtab shiftwidth=2 tabstop=2
	autocmd BufNewFile,BufRead appsettings.json,csharp setlocal expandtab
augroup END
" }}}

augroup GitGutter
	autocmd!
	autocmd BufWritePost * GitGutter
augroup END

let &t_SI="\<Esc>[6 q"
let &t_SR="\<Esc>[4 q"
let &t_EI="\<Esc>[2 q"


" ALE Options {{{
let g:ale_linters = {}
let g:ale_enabled = 1

" ALE::linters::kotlin
let g:ale_linters.kotlin = []
" call extend(g:ale_linters.kotlin, ['kotlinc'])
call extend(g:ale_linters.kotlin, ['ktchk'])
call extend(g:ale_linters.kotlin, ['languageserver'])
let g:ale_kotlin_languageserver_executable = ''

" ALE::linters::rust
let g:ale_linters.rust = []
call extend(g:ale_linters.rust, ['cargo', 'rustc'])

" ALE::linters::java
let g:ale_linters.java = []
call extend(g:ale_linters.java, ['javac'])

" ALE::linters::mcsc
let g:ale_cs_mcs_options = '-pkg:dotnet'

let g:ale_sign_error = '▪'   " u+23f5
let g:ale_sign_warning = '⦁' " u+2981

let g:ale_warn_about_trailing_whitespace = 0

let g:ale_kotlin_kotlinc_enable_config = 1
let g:ale_kotlin_kotlinc_config_file = '.ale_kotlinc_config'
let g:ale_kotlin_ktlint_enable_config = 1
" }}}

" Rust
let g:rust_recommended_style = 0

" FZF {{{
let g:fzf_colors = {
	\ 'fg':      ['fg', 'Normal'],
	\ 'bg':      ['bg', 'Normal'],
	\ 'hl':      ['fg', 'Comment'],
	\ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
	\ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
	\ 'hl+':     ['fg', 'Statement'],
	\ 'info':    ['fg', 'PreProc'],
	\ 'border':  ['fg', 'Ignore'],
	\ 'prompt':  ['fg', 'Conditional'],
	\ 'pointer': ['fg', 'Exception'],
	\ 'marker':  ['fg', 'Keyword'],
	\ 'spinner': ['fg', 'Label'],
	\ 'header':  ['fg', 'Comment']
\}

let g:fzf_layout = { 'down': '~30%' }
" }}}

" FZF Mappings {{{
nnoremap <silent> <leader>fb :Buffers<cr>
nnoremap <silent> <leader>ff :Files<cr>
nnoremap <silent> <leader>fm :Marks<cr>
nnoremap <silent> <leader>fc :Colors<cr>
" }}}

" Astyle
let g:mvars_astyle_command = 'astyle'
let g:mvars_astyle_format = '-xn -xc -C -xG -S -K -y -A2 -t4 -j -z2 -H -p -m0'

" Format the current buffer using astyle {{{
function! MyFuncAstyleFormat()
	silent !clear
	execute '%! ' . g:mvars_astyle_command . ' ' . g:mvars_astyle_format
endfunction
" }}}

" GitGutter configuration {{{
let g:gitgutter_sign_added='│'
let g:gitgutter_sign_modified='│'
let g:gitgutter_sign_removed='⏵'
let g:gitgutter_sign_removed_first_line='⏵'
let g:gitgutter_sign_modified_removed='⏵'
" let g:gitgutter_sign_added='┃'
" let g:gitgutter_sign_modified='┃'
" }}}

" Visual mode search {{{
" http://got-ravings.blogspot.com/2008/07/vim-pr0n-visual-search-mappings.html
function! s:VSetSearch()
	let temp = @@
	norm! gvy
	let @/ = '\V' . substitute(escape(@@, '\'), '\n', '\\n', 'g')
	let @@ = temp
endfunction

vnoremap * :<C-u>call <SID>VSetSearch()<CR>//<CR>
vnoremap # :<C-u>call <SID>VSetSearch()<CR>??<CR>
"}}}

" vim: foldmethod=marker filetype=vim
