let s:conf = {}
let s:conf.windows = {"font": 'Fantasque_Sans_Mono:h12', "linespace": '0'}
let s:conf.others = {"font": 'Fantasque\ Sans\ Mono\ 12', "linespace": '0'}

let s:platform_conf = s:conf.others

if has('gui_win32')
	let s:platform_conf = s:conf.windows
endif

exec "set guifont=" . s:platform_conf.font
exec "set linespace=" . s:platform_conf.linespace
set visualbell t_vb=

set lines=50
set columns=100

set guiheadroom=0
set guioptions-=e
set guioptions-=T
set guioptions-=m
set guioptions-=l
set guioptions-=L
set guioptions-=r
set guioptions-=R
