scriptencoding utf-8

let g:statusline_ff_map = {
	\ 'unix': ' LF ',
	\ 'dos':  ' CRLF ',
	\ 'mac':  ' CR '
\}

let g:statusline_mode_map = {
	\ '__': '  -  ',
	\ 'n':  '  N  ',
	\ 'i':  '  I  ',
	\ 'R':  '  R  ',
	\ 'c':  '  C  ',
	\ 'v':  '  V  ',
	\ 'V':  ' V-L ',
	\ '': ' V-B ',
	\ 's':  '  S  ',
	\ 'S':  '  S  ',
	\ '': '  S  ',
\}

function! statusline#mode()
	try
		let l:mode = mode()
		return g:statusline_mode_map[l:mode]
	catch
		return '  -  '
	endtry
endfunction

function! statusline#sep(space_around)
	return a:space_around ? ' | ' : '|'
endfunction


function! statusline#fileformat()
	return g:statusline_ff_map[&fileformat]
endfunction

function! statusline#filetype()
	let l:filetype = &filetype
	return ' ' . (&filetype!=#'' ? &filetype : 'no-ft') . ' '
endfunction

" Functionality dependent on external plugins {{{
function! statusline#ale()
	try
		let l:status_line = ale#statusline#Count(bufnr('%'))
		let l:nearest = ale#loclist_jumping#FindNearest('after', 1)

		let l:st = 'I:' . l:status_line['info']
			\ . ' W:' . l:status_line['warning']
			\ . ' E:' . l:status_line['error']

		let l:st = !empty(l:nearest) ? (l:st . ' ∙ ' . l:nearest[0]) : l:st
		return ' [' . l:st . '] '
	catch
		return ''
	endtry
endfunction

function! statusline#scm_head()
	try
		let l:head = fugitive#head()
		return  l:head !=# '' ? '  '. l:head . ' ' : ''
	catch
		return ''
	endtry
endfunction
" }}}
